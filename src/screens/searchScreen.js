import React from 'react';
import {
  View,
  ToastAndroid,
  StyleSheet,
  Dimensions,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import SearchBar from '../components/SearchBar';
import {connect} from 'react-redux';
import {
  searchTextChanged,
  getImages,
  setNumberOfColumns,
  loadMoreImages,
} from '../actions';
import '../global.js';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import ListFooter from '../components/ListFooter';
import NetInfo from '@react-native-community/netinfo';
import {isNetConnected, setInternetStatus} from '../global.js';
import ProgressDialog from '../components/ProgressDialog';

const MoreIcon = require('../../assets/more.png');
var num = 2;
var pageNum = 1;
var callSetNumberOfColumns = () => {};

const handleFirstConnectivityChange = state => {
  NetInfo.removeEventListener(handleFirstConnectivityChange);
  setInternetStatus(state.isInternetReachable);
};

const CheckConnectivity = async () => {
  if (Platform.OS === 'android') {
    await NetInfo.fetch().then(state => {
      setInternetStatus(state.isInternetReachable);
      if (!isNetConnected) {
        ToastAndroid.show(
          'You are offline',
          ToastAndroid.LONG,
          ToastAndroid.CENTER,
        );
      }
    });
  }
};

const SearchScreen = props => {
  callSetNumberOfColumns = columnNum => {
    props.setNumberOfColumns(columnNum);
  };
  num = props.numOfColumns;
  return (
    <View style={styles.container}>
      <SearchBar
        searchText={props.searchText}
        onSearchTextChange={newTerm => props.searchTextChanged(newTerm)}
        onSearchTextSubmitted={() => {
          pageNum = 1;
          CheckConnectivity().then(() => props.getImages(props.searchText));
        }}
      />
      <FlatList
        style={styles.flatListStyle}
        data={props.images}
        key={props.numOfColumns}
        keyExtractor={item => item.id.toString()}
        numColumns={props.numOfColumns}
        renderItem={({item}) => {
          return (
            <View>
              <Image
                style={{
                  width:
                    Dimensions.get('window').width / props.numOfColumns - 4,
                  height: Dimensions.get('window').width / props.numOfColumns,
                  margin: 2,
                }}
                source={{
                  uri: isNetConnected ? item.url_s : item.url_s,
                }}
              />
            </View>
          );
        }}
        onEndReached={() => {
          if (isNetConnected) {
            NetInfo.fetch().then(state => {
              setInternetStatus(state.isInternetReachable);
              if (isNetConnected) {
                pageNum = pageNum + 1;
                props.loadMoreImages(props.searchText, pageNum);
              } else {
                pageNum = 1;
                props.getImages(props.searchText);
              }
            });
          } else {
            CheckConnectivity().then(() => {
              pageNum = 1;
              props.getImages(props.searchText);
            });
          }
        }}
        onEndReachedThreshold={0.5}
        ListFooterComponent={props.images.length === 0 ? null : ListFooter}
      />
      <ProgressDialog loading={props.loading} />
    </View>
  );
};

SearchScreen.navigationOptions = ({navigation}) => {
  return {
    headerRight: (
      <Menu>
        <MenuTrigger>
          <Image style={styles.menuIconStyle} source={MoreIcon} />
        </MenuTrigger>
        <MenuOptions>
          <MenuOption
            style={styles.menuItemStyle}
            onSelect={() => callSetNumberOfColumns(2)}
            text="2 Columns"
          />
          <MenuOption
            style={styles.menuItemStyle}
            onSelect={() => callSetNumberOfColumns(3)}
            text="3 Columns"
          />
          <MenuOption
            style={styles.menuItemStyle}
            onSelect={() => callSetNumberOfColumns(4)}
            text="4 Columns"
          />
        </MenuOptions>
      </Menu>
    ),
  };
};

const mapStateToProps = state => {
  return {
    searchText: state.search.textToSearch,
    images: state.search.images,
    loading: state.search.loading,
    numOfColumns: state.search.numOfColumns,
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F3F3F3',
  },
  image: {
    width: Dimensions.get('window').width / num - 4,
    height: Dimensions.get('window').width / num,
    margin: 2,
  },
  flatListStyle: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 15,
  },
  menuItemStyle: {height: 50},
  menuIconStyle: {height: 40, width: 40},
});

export default connect(
  mapStateToProps,
  {searchTextChanged, getImages, setNumberOfColumns, loadMoreImages},
)(SearchScreen);
