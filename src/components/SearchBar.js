import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
const SearchBar = ({searchText, onSearchTextChange, onSearchTextSubmitted}) => {
  return (
    <View style={styles.background}>
      <FontAwesomeIcon
        icon={faSearch}
        style={styles.iconStyle}
        color="black"
        size={30}
      />
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        style={styles.inputStyle}
        placeholder="Search"
        value={searchText}
        onChangeText={newTerm => onSearchTextChange(newTerm)}
        onEndEditing={onSearchTextSubmitted}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    marginTop: 10,
    backgroundColor: '#f0eeee',
    height: 50,
    borderRadius: 5,
    marginHorizontal: 15,
    flexDirection: 'row',
  },
  inputStyle: {
    borderColor: 'black',
    fontSize: 18,
    flex: 1,
  },
  iconStyle: {
    alignSelf: 'center',
    marginHorizontal: 15,
  },
});

export default SearchBar;
