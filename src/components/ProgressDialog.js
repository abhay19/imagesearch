import {Modal, ProgressBarAndroid, View, StyleSheet} from 'react-native';
import React from 'react';

const ProgressDialog = props => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.loading}
      onRequestClose={() => {}}>
      <View style={styles.loading}>
        <ProgressBarAndroid color="#2196F3" styleAttr="Large" />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ProgressDialog;
