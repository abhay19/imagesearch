import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';

const ListFooter = () => {
  return (
    <View
      style={{
        position: 'relative',
        height: 50,
        paddingVertical: 20,
        borderTopWidth: 1,
        marginTop: 10,
        marginBottom: 10,
      }}>
      <ActivityIndicator animating size="large" />
    </View>
  );
};

export default ListFooter;
