import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';
import SearchScreen from './screens/searchScreen';
import {MenuProvider} from 'react-native-popup-menu';

const navigation = createStackNavigator(
  {
    Search: SearchScreen,
  },
  {
    initialRouteName: 'Search',
    defaultNavigationOptions: {
      title: 'Image Search',
    },
  },
);

const App = createAppContainer(navigation);
export default () => {
  return (
    <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
      <MenuProvider>
        <App />
      </MenuProvider>
    </Provider>
  );
};
