import {
  SEARCH_TEXT_CHANGE,
  LOADING,
  SET_IMAGES,
  SET_COLUMN_NUM,
  LOAD_MORE_IMAGES,
} from './types';
import axios from 'axios';
import {Platform, ToastAndroid} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';
import {AsyncStorage} from 'react-native';
import {isNetConnected} from '../global.js';
import RNFS from 'react-native-fs';

const FLICKR_API_KEY = 'dbfa215f050cb6a742a749aebc15cf4c';

export const searchTextChanged = text => {
  return {
    type: SEARCH_TEXT_CHANGE,
    payload: text,
  };
};

export const setNumberOfColumns = num => {
  return {
    type: SET_COLUMN_NUM,
    payload: num,
  };
};

const storeData = async (searchKey, images) => {
  try {
    await AsyncStorage.setItem(searchKey, JSON.stringify(images));
  } catch (error) {
    // Error saving data
  }
};

const storeMoreData = async (searchKey, image) => {
  try {
    await getData(searchKey.toString().toUpperCase())
      .then(images => {
        var images = JSON.parse(images).photos;
        images.push(image);
        AsyncStorage.setItem(searchKey, JSON.stringify({photos: images}));
      })
      .catch(e => {
        // error
        console.log(e);
      });
  } catch (error) {
    // Error saving data
  }
};

const getData = async searchKey => {
  let images = '';
  try {
    images = (await AsyncStorage.getItem(searchKey)) || 'none';
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
  return images;
};

export const getImages = text => {
  return dispatch => {
    dispatch({
      type: LOADING,
      payload: true,
    });
    if (isNetConnected) {
      const urlEndpoint = `https://api.flickr.com/services/rest/? method=flickr.photos.search&api_key=${FLICKR_API_KEY}&format=json&text=${text}&nojsoncallback=true&per_page=50&extras=url_s`;
      axios
        .get(urlEndpoint)
        .then(response => {
          console.log(response);
          dispatch({
            type: SET_IMAGES,
            payload: response.data.photos.photo,
          });
          var index = 0;
          var offlinePhotos = [];
          response.data.photos.photo.forEach(photo => {
            // ImgToBase64.getBase64String(photo.url_s)
            //   .then(base64String => {
            //     offlinePhotos.push({id: photo.id, url_s: base64String});
            //     storeData(text.toString().toUpperCase(), {photos: offlinePhotos});
            //   })
            //   .catch(err => {
            //     console.log(err);
            //   });

            // RNFetchBlob.config({
            //   // add this option that makes response data to be stored as a file,
            //   // this is much more performant.
            //   fileCache : true,
            //   appendExt : 'jpg',
            // }).fetch('GET', photo.url_s)
            //   // when response status code is 200
            //   .then(res => {
            //     // the conversion is done in native code
            //    // let base64String = res.base64();
            //     offlinePhotos.push({id: photo.id, url_s: res.path()});
            //     storeData(text.toString().toUpperCase(), {
            //       photos: offlinePhotos,
            //     });
            //   })
            //   // Status code is not 200
            //   .catch((errorMessage, statusCode) => {
            //     // error handling
            //     console.log(errorMessage);
            //   });

            const name = `${photo.id}`;
            const extension = Platform.OS === 'android' ? 'file://' : '';
            const path = `${extension}${RNFS.CachesDirectoryPath}/${name}.jpg`; //U can use any format png, jpeg, jpg

            RNFS.exists(path).then(exists => {
              RNFS.downloadFile({fromUrl: photo.url_s, toFile: path})
                .promise.then(res => {
                  console.log(path);
                  offlinePhotos.push({id: photo.id, url_s: path});
                  storeData(text.toString().toUpperCase(), {
                    photos: offlinePhotos,
                  });
                })
                .catch(error => {
                  console.warn(error);
                });
            });
          });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      getData(text.toString().toUpperCase())
        .then(images => {
          if (images === 'none' || images === '') {
            dispatch({
              type: LOADING,
              payload: false,
            });
            ToastAndroid.show('No Results found', ToastAndroid.LONG);
          } else {
            dispatch({
              type: SET_IMAGES,
              payload: JSON.parse(images).photos,
            });
          }
        })
        .catch(e => {
          // error
          console.log(e);
        });
    }
  };
};

export const loadMoreImages = (text, pageNum) => {
  return dispatch => {
    const urlEndpoint = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${FLICKR_API_KEY}&format=json&text=${text}&nojsoncallback=true&per_page=50&extras=url_s&page=${pageNum}`;
    axios
      .get(urlEndpoint)
      .then(response => {
        console.log(response);
        dispatch({
          type: LOAD_MORE_IMAGES,
          payload: response.data.photos.photo,
        });
        response.data.photos.photo.forEach(photo => {
          // ImgToBase64.getBase64String(photo.url_s)
          //   .then(base64String => {
          //     offlinePhotos.push({id: photo.id, url_s: base64String});
          //     storeData(text.toString().toUpperCase(), {photos: offlinePhotos});
          //   })
          //   .catch(err => {
          //     console.log(err);
          //   });

          // RNFetchBlob.config({
          //   // add this option that makes response data to be stored as a file,
          //   // this is much more performant.
          //   fileCache: true,
          //   appendExt: 'jpg',
          // })
          //   .fetch('GET', photo.url_s)
          //   // when response status code is 200
          //   .then(res => {
          //     // the conversion is done in native code
          //     // let base64String = res.base64();
          //     storeMoreData(text.toString().toUpperCase(), {
          //       id: photo.id,
          //       url_s: res.path(),
          //     });
          //   })
          //   // Status code is not 200
          //   .catch((errorMessage, statusCode) => {
          //     // error handling
          //     console.log(errorMessage);
          //   });

          const name = `${photo.id}`;
          const extension = Platform.OS === 'android' ? 'file://' : '';
          const path = `${extension}${RNFS.CachesDirectoryPath}/${name}.jpg`; //U can use any format png, jpeg, jpg

          RNFS.exists(path).then(exists => {
            RNFS.downloadFile({fromUrl: photo.url_s, toFile: path})
              .promise.then(res => {
                console.log(path);
                storeMoreData(text.toString().toUpperCase(), {
                  id: photo.id,
                  url_s: path,
                });
              })
              .catch(error => {
                console.warn(error);
              });
          });
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
};
