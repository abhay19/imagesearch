export const SEARCH_TEXT_CHANGE = 'search_text_change';
export const LOADING = 'loading';
export const SET_IMAGES = 'set_images';
export const LOAD_MORE_IMAGES = 'load_more_images';
export const SET_COLUMN_NUM = 'set_column_num';
