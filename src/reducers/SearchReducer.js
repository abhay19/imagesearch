import {
  SEARCH_TEXT_CHANGE,
  LOADING,
  SET_IMAGES,
  SET_COLUMN_NUM,
  LOAD_MORE_IMAGES,
} from '../actions/types';

const INITIAL_STATE = {
  images: [],
  textToSearch: '',
  loading: false,
  numOfColumns: 2,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SEARCH_TEXT_CHANGE: {
      return {...state, textToSearch: action.payload};
    }
    case SET_IMAGES: {
      return {...state, images: action.payload, loading: false};
    }
    case LOAD_MORE_IMAGES: {
      return {...state, images: state.images.concat(action.payload)};
    }
    case SET_COLUMN_NUM: {
      return {...state, numOfColumns: action.payload};
    }
    case LOADING: {
      return {...state, loading: action.payload};
    }
    default:
      return state;
  }
};
